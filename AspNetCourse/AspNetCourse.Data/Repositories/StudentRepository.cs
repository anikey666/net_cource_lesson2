﻿using AspNetCourse.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCourse.Data.Repositories
{
    public class StudentRepository 
        : RepositoryBase<Student>
    {
        public StudentRepository(CourseDbContext dbContext) 
            : base(dbContext)
        {
        }
    }
}
