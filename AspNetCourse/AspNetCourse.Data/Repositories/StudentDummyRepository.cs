﻿using AspNetCourse.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCourse.Data.Repositories
{
    public class StudentDummyRepository 
        : RepositoryBase<Student>
    {
        public StudentDummyRepository(CourseDbContext dbContext)
            : base(dbContext)
        {
        }

        public override IQueryable<Student> GetAll()
        {
            return (new Student[] { }).AsQueryable();
        }
    }
}
