﻿using AspNetCourse.Data.Models;
using System.Linq;

namespace AspNetCourse.Data.Repositories
{
    public interface IRepository<TEntity> 
        where TEntity : EntityBase
    {
        TEntity Get(long id);
        IQueryable<TEntity> GetAll();
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(long id);
    }
}
