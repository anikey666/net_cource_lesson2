﻿using AspNetCourse.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCourse.Data.Repositories
{
    public abstract class RepositoryBase<TEntity> 
        : IRepository<TEntity>
        where TEntity : EntityBase
    {
        #region constructors
        public RepositoryBase(CourseDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        #endregion

        #region public methods
        public TEntity Get(long id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }
        public TEntity Add(TEntity entity)
        {
            var result = DbSet.Add(entity);
            _dbContext.SaveChanges();
            return result;
        }
        public TEntity Update(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
            return entity;
        }
        public void Delete(long id)
        {
            var entity = Get(id);
            DbSet.Remove(entity);
            _dbContext.SaveChanges();
        }
        #endregion

        #region protected fields and properties
        protected readonly CourseDbContext _dbContext;
        protected DbSet<TEntity> DbSet => _dbContext.Set<TEntity>();
        #endregion
    }
}
