﻿namespace AspNetCourse.Data.Models
{
    public abstract class EntityBase
    {
        public long Id { get; set; }
    }
}
