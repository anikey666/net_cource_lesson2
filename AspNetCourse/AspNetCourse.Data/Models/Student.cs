﻿using System.ComponentModel.DataAnnotations;

namespace AspNetCourse.Data.Models
{
    public class Student 
        : EntityBase
    {
        [Display (Name ="First Name")]
        [Required]
        [MaxLength(20, ErrorMessage = "To mach letters")]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public int Age { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}
