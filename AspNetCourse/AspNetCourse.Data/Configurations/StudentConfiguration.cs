﻿using AspNetCourse.Data.Models;
using System.Data.Entity.ModelConfiguration;

namespace AspNetCourse.Data.Configurations
{
    internal class StudentConfiguration 
        : EntityTypeConfiguration<Student>
    {
        public StudentConfiguration()
        {
            ToTable("Student");

            HasKey(x => x.Id);

            Property(x => x.FirstName).HasMaxLength(100).IsRequired();
            Property(x => x.LastName).HasMaxLength(100).IsRequired();
            Property(x => x.Email).HasMaxLength(100).IsRequired();
            Property(x => x.Age).IsRequired();
        }
    }
}
