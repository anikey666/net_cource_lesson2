﻿using AspNetCourse.Data.Configurations;
using System.Data.Entity;

namespace AspNetCourse.Data
{
    public class CourseDbContext 
        : DbContext
    {
        public CourseDbContext(string connectionString)
            : base(connectionString)
        {
        }
        public CourseDbContext() 
            : base()
        {
        }

        public DbSet<Models.Student> Students { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentConfiguration());
        }
    }
}
