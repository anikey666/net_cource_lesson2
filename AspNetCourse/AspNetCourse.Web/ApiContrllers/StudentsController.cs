﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AspNetCourse.Data;
using AspNetCourse.Data.Models;
using AspNetCourse.Data.Repositories;

namespace AspNetCourse.Web.ApiContrllers
{
    public class StudentsController : ApiController
    {
        public StudentsController(IRepository<Student> repo)
        {
            _repo = repo;
        }

        //private CourseDbContext db = new CourseDbContext();

        // GET: api/Students
        public IQueryable<Student> GetStudents()
        {
            return _repo.GetAll();
        }

        private readonly IRepository<Student> _repo;
    }
}