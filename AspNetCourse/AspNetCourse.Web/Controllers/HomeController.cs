﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNetCourse.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        public ActionResult Some()
        {
            return View();
        }

        public PartialViewResult SomePatial()
        {
            return PartialView(DateTime.Now);
        }

    }
}
