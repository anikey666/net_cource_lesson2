﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetCourse.Web.Models
{
    public class StudentListItemModel
    {
        public long Id { get; set; }
        public string FullName { get; set; }
    }
}