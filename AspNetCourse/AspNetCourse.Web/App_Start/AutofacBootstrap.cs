﻿using AspNetCourse.Data;
using AspNetCourse.Data.Models;
using AspNetCourse.Data.Repositories;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AspNetCourse.Web.App_Start
{
    public static class AutofacBootstrap
    {
        public static IContainer BuildContainer()
        {
            //создание билдера контейнера
            var builder = new ContainerBuilder();

            //MVC котроллеры
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //WebAPI котроллеры
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //прочие зависимости
            builder.RegisterType<StudentDummyRepository>()
                .As<IRepository<Student>>()
                .InstancePerRequest();

            builder.RegisterType<CourseDbContext>()
                .AsSelf()
                .InstancePerRequest();

            //непосредственно создание контейнера
            var container = builder.Build();

            //регстрация контейнера для MVC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            return container;
        }
    }
}