﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lesson2.Courses;

namespace Lesson2.People
{
    /// <summary>
    /// Our Student
    /// </summary>
    class Student : Human
    {
        public Student()
        {
            
        }

        public void DoSomething()
        {
            //кинем наше кастомное исключение
            throw new ConfusedException("I'm little confused");
        }

        public void SubscribeOnLesson(Lesson lesson)
        {
            //вот эта переменная...
            var reaction = "run";

            lesson.LessonEvent += (e) =>
            {
                switch (e.Type)
                {
                    
                    case LessonEventArgs.EventType.Start:
                        //...замыкается в нашем анонимном методе
                        reaction = "tired";
                        Run();
                        break;
                    case LessonEventArgs.EventType.End:
                        Rest();
                        break;

                }
            };
        }

        public List<int> Filter(List<int> collection, Predicate<int> filter)
        {
            var list = new List<int>();
            foreach (var i in collection)
            {
                if (filter(i))
                {
                    list.Add(i);
                }
            }
            return list;
        }

        public override void Walk()
        {
            Console.WriteLine("Цок-цок-цок");
        }
    }

    public class ConfusedException : Exception
    {
        public ConfusedException()
            : base()
        {
        }

        public ConfusedException(string message) 
            : base(message)
        {
        }
    }
}
