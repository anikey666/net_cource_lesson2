﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2.People
{
    interface IHuman
    {
        string FirstName { get; set; }
        string LastName { get; set; }


        void Run();
        void Rest();
        void Walk();
        void Eat();
    }
}
