﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2.People
{
    abstract class Human : IHuman
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void Run()
        {
            Console.WriteLine("RUN!!!!");
        }

        public void Rest()
        {
            Console.WriteLine("Sleep zzzzzz");
        }

        public abstract void Walk();
        public virtual void Eat()
        {

        }
    }
}
