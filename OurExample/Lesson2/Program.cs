﻿using Lesson2.People;
using Lesson2.Courses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var student = new Student();
                student.DoSomething();
            }
            catch (NotImplementedException e)
            {
                //здесь мы поймаем и обработаем исключение типа NotImplementedException
                
                //в нашем случае мы сюда не попадем
            }
            catch (ConfusedException e)
            {
                //здесь мы поймаем и обработаем исключение типа NotImplementedException
                
                //как раз сюда мы и попадем
            }
            catch (Exception e)
            {
                //а тут мы обработаем любое исключение, так как все наследуются от Exception

                //но в нашем случае мы сюда не попадем, так как исключение уже обработано
            }
            finally
            {
                //блок finally выполнится в любом случае, даже если ошибки не было.
                //используется для написания кода завершения процессов 
                //и высвобождения объектов
            }
        }
    }
}
