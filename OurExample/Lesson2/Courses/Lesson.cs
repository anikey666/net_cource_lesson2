﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2.Courses
{
    public class Lesson
    {
        #region constructors
        public Lesson(string name, DateTime startDate, DateTime endDate)
        {
            Name = name;
            _startDate = startDate;
            _endDate = endDate;
        }

        public Lesson()
        {
            _startDate = DateTime.Now;
            _endDate = _startDate.AddMinutes(90);
            this.StartLesson();
        }
        #endregion

        #region properties
        public string Name { get; set; }
        public DateTime StartDate
        {
            get
            {
                return _startDate;
            }
        }

        public DateTime StartDateUtc
        {
            get
            {
                return _startDate.ToUniversalTime();
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                if (value <= StartDate)
                {
                    throw new Exception();
                }
                _endDate = value;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return _endDate - StartDate;
            }
        }
        #endregion

        #region methods
        public void StartLesson()
        {
            if (_isStarted)
            {
                return;
            }
            _isStarted = true;
            if (LessonEvent != null)
            {
                LessonEvent(new LessonEventArgs() { Type = LessonEventArgs.EventType.Start });
            }
        }

        public void EndLesson()
        {
            LessonEvent(new LessonEventArgs() { Type = LessonEventArgs.EventType.End });
        }
        #endregion

        #region events
        public event LessonEvent LessonEvent;
        #endregion

        #region private fields
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _isStarted = false;
        #endregion
    }

    #region service classes
    public delegate void LessonEvent(LessonEventArgs e);
    public class LessonEventArgs : EventArgs
    {
        public EventType Type { get; set; }

        public enum EventType
        {
            Start,
            End
        }
    }
    #endregion
}
