﻿using System.Collections.Generic;

namespace Lesson2.Courses
{
    public class Course
    {
        public Dictionary<string, Lesson> Lessons { get; set; }
    }
}
