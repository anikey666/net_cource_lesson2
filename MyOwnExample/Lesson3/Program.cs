﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Lesson3
{
    class Program
    {
        //делегат определяет сигнатуру метода: 
        //тип возвращаемого значения и входные параметры
        delegate void DoSomething();
        delegate void ImportantEvent(EventArgs e);

        static void Main(string[] args)
        {
            Console.WriteLine("Выберите число от 1 до 10");
            int inputNumber;
            while (!int.TryParse(Console.ReadLine(), out inputNumber))
            {
                Console.WriteLine("Неправильно. повторите ввод");
            }


            var i = 10;
            //условные операторы могут иметь разный набор блоков
            if (i < 40)
                Console.WriteLine("Меньше 40"); //в каждой ветке условного оператора допускается лишь один оператор

            if (i < 40)
            {
                //если действий нужно много достаточно указать блок операторов через { }
                Console.WriteLine("Меньше 40");
            }
            else
            {
                Console.WriteLine("Больше или равно 40");
            }

            if (i < 40)
            {
                Console.WriteLine("Меньше 40");
            }
            else if (i == 40) //можно несколько блоков else if
            {
                Console.WriteLine("авно 40");
            }
            {
                Console.WriteLine("Больше или равно 40");
            }

            //тернарный оператор
            Console.WriteLine((i == 40) ? "Равно 40" : "Не равно 40");

            //рассмотрим подробнее
            //тернарый оператор возвращает значение исходя из условия
            Console.WriteLine((i == 40) ?   //условие идет сначала, отделяется знаком ?
                    "Равно 40"              //тут у нас то значение, которое будет возвращено если условие верно
                    : "Не равно 40");       //после этого ставим : и пишем то, что будет возвращено, если условие ложно
            //важно понимать, что терный оператор используется именно для того, чтобы вернуть значение, 
            //а не просто выполнить набор действий


            var condition = false;

            //проверяем условие...
            while (condition)
            {
                //и пока оно истинно, выполняем действие
                DoAction();
            }
            //цикл может ни разу не выполниться

            //выполняем действие...
            do
            {
                DoAction();
            }
            while (condition); //... а потом проверяем условие
            //в результате хотя бы раз цикл выполнится


            for (var counter = 0; counter < 10; counter++)
            {
                Console.WriteLine(counter);
            }


            var d6Result = 1;

            //оператор выбора
            switch (d6Result)
            {
                //в метках case определяются константы, в зависиомсти от
                //значения входной переменной запустится нужная ветка
                //в более поздних версиях допускается использование условий (разберем на других занятиях)
                case 1:
                    Console.WriteLine("Критический промах");
                    break; //обязателем в C#, не дает пройтись дальше по веткам.
                           //может быть заменен на return.
                case 2: //ветки можно объединять по несколько меток case
                case 3:
                    Console.WriteLine("Шансы маленькие");
                    break;
                case 4:
                case 5:
                    Console.WriteLine("Шансы большие");
                    break;
                case 6:
                    Console.WriteLine("Критический успех!!!");
                    break;
                default: //метка default не обязательна, но дает шанс обработать случай,
                    //если ни одна метка case не сработала
                    Console.WriteLine("Ты кинул не D6");
                    break;

            }

            var numbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //цикл foreach позволяет пройтись по элементам массива или коллекции
            foreach (var curNumber in numbers)
            {
                Console.WriteLine(curNumber);
            }

            //однако под капотом не все так просто. объект должен реализовывать IEnumerable<T>
            Console.WriteLine(numbers is IEnumerable<int>);

            //в нем есть метод IEnumerator GetEnumerator();
            // и именно он обеспечивает работу foreach

            //список
            var list = new List<int>();
            //словарь
            var dictionary = new Dictionary<int, string>();
            //очередь
            var queue = new Queue<int>();
            //стэк
            var stack = new Stack();
            //...... и другие......

            list = new List<int>();         //можно создать пустой...
            list.Add(1);                    //... а потом наполнить
            list = new List<int>(numbers);  //можно создать на основе существующего
            //а можно методами расширений
            dictionary = list.ToDictionary(x => x, x => x.ToString());



            //создадим объект делегата
            DoSomething doSomething;
            //добавим туда метод
            doSomething = DoAction;
            //еще один
            doSomething += DoAction2;

            //и еще один, только теперь анонимный
            doSomething = delegate ()
            {
                Console.WriteLine("Do 3");
            };

            //а теперь вызовем все 3 метода!
            doSomething();


            //предикат имеет обобщенны параметр входной переменной и возвращает bool
            Predicate<int> predicate = (number) => { return number > 10; };
            //Action имеет входные обобщенные параметры (до 16) и производит
            //с ними действия без вывода результата
            Action<int, int> action = (a1, a2) => { Console.WriteLine(a1 + a2); };
            //Function имеет входные обобщенные параметры (до 16) и обобщенный параметр
            //результата. Соответственно производит действие и возвращает некий результат
            Func<int, int> func = (number) => { return number++; };


            try
            {
                var holder = new OBjectHolder<Cube>();
                //тут произойдет ошибка
                var cube = holder.WithdrawObject();
            }
            catch (NotImplementedException e)
            {
                //здесь мы поймаем и обработаем исключение типа NotImplementedException

                //в нашем случае мы сюда не попадем
            }
            catch (WithdrawException e)
            {
                //здесь мы поймаем и обработаем исключение типа NotImplementedException

                //как раз сюда мы и попадем
            }
            catch (Exception e)
            {
                //а тут мы обработаем любое исключение, так как все наследуются от Exception

                //но в нашем случае мы сюда не попадем, так как исключение уже обработано
            }
            finally
            {
                //блок finally выполнится в любом случае, даже если ошибки не было.
                //используется для написания кода завершения процессов 
                //и высвобождения объектов
            }

        }

        public static void DoAction()
        {
            Console.WriteLine("Do 1");
        }

        public static void DoAction2()
        {
            Console.WriteLine("Do 2");
        }
    }

    /// <summary>
    /// Обобщения позволяют применить одни алгоритмы на множество типов
    /// Бывают обобщения на уровне классов
    /// </summary>
    /// <typeparam name="TObject">Данный параметр именно для обобщения всего класса</typeparam>
    public class OBjectHolder<TObject>
        where TObject : class
    {
        //Переданный параметр может использоваться внутри класса, как строгий тип
        //можно создавать поля и свойства
        private TObject _holdingObject;

        //Можно использовать в методах.
        public TObject WithdrawObject()
        {
            if (_holdingObject != null)
            {
                var forReturn = _holdingObject;
                _holdingObject = null;

                return forReturn;
            }
            throw new WithdrawException("Nothing to withdraw");
        }

        //Но также можно делать обобщения и в отдельных методах
        public bool TryHold<TCheckObject>(TCheckObject obj)
            where TCheckObject : class
        {
            if (obj is TObject)
            {
                _holdingObject = obj as TObject;
                return true;
            }
            return false;
        }
    }

    public class Cube
    {
    }

    /// <summary>
    /// Исключения можно создавать свои собственные
    /// </summary>
    public class WithdrawException : Exception
    {
        public WithdrawException() 
            : base()
        {
        }

        public WithdrawException(string message)
            : base(message)
        {
        }
    }
}
