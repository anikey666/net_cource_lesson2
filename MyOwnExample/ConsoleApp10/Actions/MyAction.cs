﻿using System;
using TestProgram.Models;

namespace TestProgram.Actions
{
    /// <summary>
    /// 
    /// </summary>
    public class MyAction : IDisposable
    {
        public void DoAction1()
        {
            var myData1 = new Models.MyData();     //создаем объект
            myData1.Name = "obj 1";         //заполянем имя
            myData1.Value = 1;              //заполянем значение

            //а теперь создадим через конструктор с параметрами
            var myData2 = new MyData("obj 2", 2);

            //прибавим к одному другой...
            myData1.Add(myData2);
            //... и посмотрим результат
            Console.WriteLine($"Результат сложения: {myData1}");

            //а теперь посмотрим на область видимости
            var myNumber = 5;
            myData2 = DoAction2(myData1, myNumber);
        }

        public MyData DoAction2(MyData myData, int myNumber)
        {
            //изменим myData
            myData.Value = myNumber;
            //изменим myNumber
            myNumber++;

            //создадим еще 2 объекта и посмотрим когда они уничтожатся
            var testMyData1 = new MyData() { Name = "Test 1", Value = 1 };
            var testMyData2 = new MyData() { Name = "Test 2", Value = 1 };

            return testMyData1;
        }

        /// <summary>
        /// Данный метод предназначен для ручной очистки объектов, инкапсулированных в данный класс.
        /// В нашем примере он вызовется неявно после окончания блока using
        /// </summary>
        public void Dispose()
        {
        }
    }
}
