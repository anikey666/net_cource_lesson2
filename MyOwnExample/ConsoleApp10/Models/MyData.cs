﻿using System;

namespace TestProgram.Models
{
    /// <summary>
    /// Тестовый класс
    /// </summary>
    public class MyData
    {
        #region properties and fields
        /// <summary>
        /// Берет или задает имя объекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Берет или задает значение объекта
        /// </summary>
        public int Value { get; set; }
        #endregion

        #region constructors
        /// <summary>
        /// Инициализирует новый экземпляр <seealso cref="MyData"/>.
        /// </summary>
        public MyData()
        {
            Console.WriteLine($"{this.ToString()} был создан");
        }

        /// <summary>
        /// Инициализирует новый экземпляр  <seealso cref="MyData"/>.
        /// </summary>
        /// <param name="name">Имя объекта.</param>
        /// <param name="value">Значение объекта.</param>
        public MyData(string name, int value)
        {
            Name = name;
            Value = value;

            Console.WriteLine($"{this.ToString()} был создан");
        }
        #endregion

        #region destructor
        /// <summary>
        /// Деструктор. 
        /// </summary>
        ~MyData()
        {
            //однако в нашем случае вы его ни разу не увидите, так как сборщик мусора ни разу не сработает, 
            //в его работе нет необходимости, так как загрузки по ресурсам нет
            Console.WriteLine($"{this.ToString()} был удален");
        }
        #endregion

        #region public methods
        /// <summary>
        /// Gets the string representation of the object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            //пример интерполяции строк (C# 6.0), очень удобно вместо громоздкого string.Format(...)
            return $"MyData объект с именем \"{Name}\" и значением \"{Value}\"";
        }

        public void Add(MyData anotherObj)
        {
            Name = $"{Name}; {anotherObj.Name}";
            Value += anotherObj.Value;

        }
        #endregion

    }
}
