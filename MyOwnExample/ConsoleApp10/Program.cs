﻿using System;

namespace TestProgram
{
    

    /// <summary>
    /// Комментарии для автодокументации
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //строчный комментарий

            /*Блочный
             комментарий*/
            
            //доступ к классам с одним именем, но в разных пространствах имен
            var data1 = new Models.MyData(); //создание объекта через пустой конструктор, но созданный вручную
            var data2 = new AnotherModels.MyData(); //создание объекта через пустой коснтруктор по умолчанию


            //определение контекста жизни объекта через конструкцию using
            using (var actions = new Actions.MyAction())
            {
                //такой класс должен реализовывать интерфейс IDIsposal
                actions.DoAction1();
            }
            Console.ReadKey();
            //тут этот объект жить уже не будет.
        }
    }
}
